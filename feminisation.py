# -*- coding: utf-8 -*-
"""
féminisation
"""
import pandas as pd

feminises = dict(
        Homme = 'Femme',
        Monsieur = 'Dame',
        Garçon = 'Fille', # peut être Serveuse aussi
        Valet = "Femme de chambre",
        Designer = "Designeuse",
        Dispatcher = "Dispatcheuse",
        Manager = "Manageuse",
        Booker = "Bookeuse",
        Pizzaïolo = "Pizzaïola", Toréro = "Toréra", Banderillo = "Banderilla",
        Reporter = "Reportrice",
        Steward = "Hôtesse",
        Barman = "Barmaid",
        ien = "ienne",
        er = "ère",
        on = "onne",
        ître = 'îtresse',
        ôte = 'ôtesse',
        ant = 'ante',
        man = "woman",
        el = 'elle',
        ète = "étesse", # que pour Poète
        fet = "fète", # que pour Prefet
        )
feminises['if'] = 'ive' # on fait ça parce que if est un mot clé
option_feminises = [('eur',["euse", 'rice'])]


def feminise_mot(serie):
    results = [serie + 'e']
    dico = feminises.copy()
    for masculin, feminins in option_feminises:
        for feminin in feminins:
            dico[masculin] = feminin
            new_serie = serie.copy()
            for masculin_, feminin_ in dico.items():
                new_serie = new_serie.str.replace(masculin_ + '$', feminin_)
            results.append(new_serie)
    return results

def feminisation_automatique(serie, serie_fem=None):
    pass    


if __name__ == '__main__':
    #TODO: 
    avec_feminin = lib[lib.str.contains(' / ')]
    # repère les doubles traductions féminins masculin
    double_barre = avec_feminin.str.split(' / ').str.len() == 3
    avant = avec_feminin.str.split(' / ').str[0]
    apres = avec_feminin.str.split(' / ').str[1]
    
    pattern = ' |\/|-'
    mots_avant = avant.str.split(pattern)
    mots_apres = apres.str.split(pattern)
    nb_mot_avant = mots_avant.str.len()
    for k in nb_mot_avant.unique():
        bonne_taille = nb_mot_avant == k
        mots_avant_k = mots_avant.str[:k][bonne_taille]
        mots_apres_k = mots_apres.str[:k][bonne_taille]
    

    k = 1
    bonne_taille = nb_mot_avant == k
    mots_avant_k = mots_avant.str[:k][bonne_taille]
    mots_apres_k = mots_apres.str[:k][bonne_taille]
    
    
    feminins_possibles = list()
    for idx_mot in range(k):
        mot_idx = mots_avant_k.str[idx_mot]
        feminins_possibles += feminise_mot(mot_idx)
        
    serie_fem = mots_apres_k.str[0]
    
    tout_feminins_possibles = pd.concat([x == serie_fem for x in feminins_possibles], axis=1)
    non_trouvé = tout_feminins_possibles.sum(axis=1) == 0
    print(non_trouvé.sum())
    if non_trouvé.sum() > 10:
        print(mots_avant_k[non_trouvé].head(10), mots_apres_k[non_trouvé].head(10))
    else:
        print(mots_avant_k[non_trouvé], mots_apres_k[non_trouvé])
        pd.concat(feminins_possibles, axis=1)[non_trouvé]

        
    # Arboriste grimpeur / grimpeuse
        
    probleme = avec_feminin.str.split(' / ').str[2]
    print(avec_feminin[probleme.notnull()])