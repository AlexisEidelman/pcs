 -*- coding: utf-8 -*-
"""
Created on Thu Sep 20 14:29:03 2018

@author: alexis.eidelman
"""

### Libéllés
def travaille_liste(liste):
    liste = liste.str.lower()
    for inutile in ['\(h/f\)', '\(f/h\)', '\(e\)', '\(ne\)',' hf']:
        liste = liste.str.replace(inutile, '')
    liste = liste.drop_duplicates()
    return liste

## nettoyage é, è, ç ?, '-' par espace
## stopwords

mot = "boulanger"

def uniformise_mot(mot):
    mot = mot.lower()
    return mot


def recherche(mot):
    if mot in liste:
        return mot
    echos = liste[liste.str.contains(mot)]
    if len(echos) == 0:
        print("pas d'echos")
        return
    return echos

def pour_voir(mot):
    print(" mot : ", mot)
    uniformisé = uniformise_mot(mot)
    print(" mot uniformisé : ", uniformisé)
    trouvés = recherche(uniformisé)
    print(" mots trouvés ", trouvés)


def check_liste(liste_de_reference, liste_de_teste):
    pass


pour_voir(mot)
pour_voir("chef de division")
pour_voir(mot)
pour_voir("notaire")
pour_voir("notare")

#################################
####### indice de succès ########
#################################

# variance des libellés
# variance parmi les libellés débutant beaucoup de mots
# nombre de libellés hors liste

#################################
##### cahier des charges ########
#################################

## temps de réponse, temps global de colelcte
## résitster aux cédilles, et aux
## ne pas faire rentrer trop de mots aux utilisateurs

## test du ROME

#################################
#######  ########
#################################

# nettoyage des stopwords
# pas de cédilles, d'accents
# question des sigles.
# faire le lien avec le dictionnaire

### Féminisation ####
# Dans les libellés de postes on peut mettre les deux.
# chercher le féminin.n afficher les deux simplement

if __name__ == '__main__':
    from data import read_dsn, read_compendium, read_ear_2107
    lib1 = read_dsn()
    lib2 = read_ear_2107()

    lib1 = travaille_liste(lib1)
    lib2 = travaille_liste(lib2)
