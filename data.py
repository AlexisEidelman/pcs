# -*- coding: utf-8 -*-
"""
charge toutes les données
"""
import os
import pandas as pd


def read_dsn(libellés_only=True):
    ''' Lit les données PCS et libellés '''
    #    tab = pd.read_csv('data/libelles_pcs_dsn.csv')
    if libellés_only:
        tab = pd.read_csv('data/libellé_dsn.csv', header=None)
        tab.columns = ['libellé', 'nombre']
        tab = tab.loc[tab['nombre'] > 10, 'libellé']
    else:
        tab = pd.read_csv('data/pcs_libellé_dsn.csv')
    return tab


def read_compendium(libellés_only=True):
    ''' Le compendium du pôle de besançon '''
    compendium = pd.read_excel('data//Annuaire_pole_PCS.xlsx')
    if libellés_only:
        return compendium['Libellé']
    else:
        return compendium



def read_ear_2107(libellés_only=True):
    ''' Les données du recensement '''
    path_folder = "data"
    name = 'lib100_positionEAR2017.xlsx'
    path = os.path.join(path_folder, name)
    tab = pd.read_excel(path)
    if libellés_only:
        return tab['libprof']
    return tab


def read_index(libellés_only=True):
    ''' les nouveaux libellés '''
    pass


def read_rome(libellés_only=True, rome=True):
    tab = pd.read_excel('data/ROME_ArboPrincipale.xlsx',
                        sheet_name=1, skiprows=1,
                        dtype = 'object')
    tab.columns = ['GrandDomaine', 'DomainePro', 'ROME', 'Libellé', 'Code OGR']
    tab.loc[:, 'DomainePro'] = tab.loc[:, 'DomainePro'].fillna(0).astype(int).astype(str)
    tab.loc[:, 'ROME'] = tab.loc[:, 'ROME'].fillna(0).astype(int).astype(str).str.zfill(2)
    tab.loc[:, 'code'] = tab.GrandDomaine + tab.DomainePro + tab.ROME

    if rome:
        tab = tab[tab['Code OGR'].notnull()]
        
    if libellés_only:
        lib = tab['Libellé']
        lib = lib.str.replace('Techicien', 'Technicien')
        lib = lib.str.replace('rayonneuse', 'Rayonneuse')
        lib[lib == "Tubiste / Contre tubiste"] = "Tubiste/Contre tubiste"
        return lib
    return tab


def read_appelation_rome(libellés_only=True):
    ''' c'est en fait les mêmes éléments que dans le ROME ArboPrincipale'''
    tab = pd.read_csv('data/unix_referentiel_appellation_v337_utf8.csv')
    del tab['statut'] # qui vaut toujours 1
    del tab['libelle_type_section_appellation'] # qui vaut toujours PRINCIPALE
    del tab['code_type_section_appellation'] # qui vaut toujours 1
    libs = tab['libelle_appellation_long']
    if libellés_only:
        return libs
    return tab
    
def reaf_onisep(libellés_only=True):
    tab = pd.read_csv('data/onisep.csv', sep=';')
    libs = tab['libellé métier']
    if libellés_only:
        return libs
    return tab


def read_cigref():
    cigref = pd.read_excel('data//Nomenclature du CIGREF.xlsx')
    lib = cigref.iloc[:,0]
    pattern = '\d\.\d'
    lib_sans_categorie = lib.str.contains(pattern, regex=True)


path_sies = "G:\\PMQ\\Offre de travail\\données SIES"
def read_sies(year=2014, master=True, libellés_only=True):
    file_name = 'dip' + str(year-2000) + '.sas7bdat'
    if master:
        file_name = 'master' + file_name
    else:
        file_name = 'lp' + file_name
    
    tab = pd.read_sas(os.path.join(path_sies, file_name), encoding='cp1252')
    if libellés_only:
        return tab['intitule_emploi']
    return tab