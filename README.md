# PCS Label Suggestion

It aims at suggesting users the right label for their job. 
Here is an example in French:

* Architecte BDD -> Architecte base de données
* Policier -> Policier scientifique; Policier municipal; Policier environnemental

It must suggests labels containing words typed by users. It shoud also handle 
typos and acronyms and suggest a feminine version of labels.

Eventually it will reduce post-processing time while analyzing surveys' 
results. 

To start with, we will follow popular structure for search engine:
* Build an index with all words of the documents (official labels)
* Handle queries
* Rank results (eg. tf-idf)
 
Then we must handle typos, acronyms and synonyms as well as feminine versioning 
of labels. 


