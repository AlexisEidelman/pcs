# -*- coding: utf-8 -*-
"""
Created on Thu Dec 20 16:30:18 2018

@author: claire.dumesnildemar
"""
import os
import json

## Draft, assum we have json file
## Useless class  ... To be done in a proper way --> see _morphalou.py
## L'idée en plus est de circonscrire le dictionnaire aux mots effectivement dans le lexique

class MorphalouLexicon():
    """
    Class to extend lexicon thanks to French Morphalou lexicon. Main goal is to 
    obtain a dict linking different forms of a word to the form we find in the
    documents.
    
    >>> morphalou_dict('fleurs') = 'fleur'
    >>> morphalou_dict('infirmières') = 'infirmier'
    
    //!\\ //!\\ //!\\ //!\\
    Assume there is only one form of the word in the initial lexicon.
    //!\\ //!\\ //!\\ //!\\
    
    A local copy of the Morphalou-2.0.xml file is required.
    """
    def __init__(self, lexicon = None, folder =r'C:\Users\claire.dumesnildemar\Documents\Jocas_Morphalou',
                 filename = 'morphalou_b1.json'):
        """
        lexicon     : lexicon with unique forms of words
        folder      : folder containing the Morphalou corpus
        """
        self.folder = os.path.normpath(folder)
        self.path = os.path.join(self.folder, filename)
        self._load_lemmas_dict_from_json()
        
    def load_lemmas_dictionary(self):
        """Assign a properly formated dict to the lemmas_dictionary attribute.

        If a json record of the target dictionary is found, it is loaded.
        Otherwise, the dictionary is built from xml source, adjusted and
        dumped to json so as to make future loadings faster.

        Once the dictionary has been load one way or another, skimming
        of its keys and values based on stopwords is conducted.
        """
        if os.path.exists(self.path):
            self._load_lemmas_dict_from_json()
    
    def _load_lemmas_dict_from_json(self):
        """Load a lemmas dictionary from a json file."""
        with open(self.path, 'r', encoding='utf-8') as json_file:
            self._lemmas_dictionary = json.load(json_file)