# -*- coding: utf-8 -*-
"""
Created on Mon Dec 17 11:55:10 2018

@author: claire.dumesnildemar
"""

import pandas as pd
from SearchEngine import SearchEngine
from FuzzyMatcher import FuzzyMatcher

excel_file = r'C:\Users\claire.dumesnildemar\Documents\PCS_repo\data\Annuaire_pole_PCS.xlsx'
data = pd.read_excel(excel_file, 'annuaire', usecols = ['Libellé'], squeeze = True)
export_path = 'C:/Users/claire.dumesnildemar/Documents/PCS_repo/Export_index'


se = SearchEngine(data_series=data)
#se = SearchEngine(indexes_path=export_path)
#se.create_index(data)
#se.create_revert_index()


#fm = FuzzyMatcher(lexicon_file=r'C:/Users/claire.dumesnildemar/Documents/PCS_Labels_manager/data/pcs_unique_words.txt')
#fm.get_candidates_levenshtein(['vedeur'])

#tf = TfIdfManager(data_un)