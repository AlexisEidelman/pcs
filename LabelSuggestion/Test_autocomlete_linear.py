# -*- coding: utf-8 -*-
"""
Created on Thu Dec 20 17:52:48 2018

@author: claire.dumesnildemar
"""

from bisect import bisect_left
def word_exists(wordlist, word_fragment):
    try:
#        return(wordlist[bisect_left(wordlist, word_fragment)].startswith(word_fragment))
        return(wordlist[bisect_left(wordlist, word_fragment):
             bisect_left(wordlist, word_fragment[:-1] + chr(ord(word_fragment[-1])+1))])
    except IndexError:
        return(False) # word_fragment is greater than all entries in wordlist
    

