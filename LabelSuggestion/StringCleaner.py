# -*- coding: utf-8 -*-
"""
Created on Mon Dec 17 09:51:55 2018

@author: claire.dumesnildemar
"""

import re
from MorphalouLexicon import MorphalouLexicon

class StringCleaner():
    
    def __init__(
            self, stop_words = None, keep_accent = False, 
            keep_special_char = False
            ):
        """ 
        Initialize StringCleaner:
            stop_words        : list of words to be removed from string (list)
            keep_accent       : whether to keep accents or not 
                                (bool default False)
            keep_special_char : wether to keep special characters or not 
                                (eg. ç or œ) (bool default False)
        """
        if stop_words is None:
            self.stop_words = self._default_stopwords()
        self.keep_accent = keep_accent
        self.keep_special_char = keep_special_char
        self.morphalou = MorphalouLexicon()
        
        
    def _default_stopwords(self):
        """Return a default list of stopwords. Single word only."""
        return [
                'le', 'la', 'les', 'un', 'une', 'uns', 'unes', 'des', 'du',
                'de', 'et', 'ou', 'avec', 'au', 'aux', 'que', 'qui', 'etc',
                'pour', 'pr', 'en', 'dans', 'sur', 'chez',
            ]
        
    def clean_string(self, text):
        text = text.lower()
        # Remove digits.
        text = re.sub('\d', '', text)
        # Remove ponctuation
        text = re.sub("\[|\]|\\\|\{|\}|\(|\)|\<|\>",' ', text)
        text = re.sub("\?|\.|\;|\!|\:|\,", ' ', text)
        text = re.sub("\'|\’|\"|\`", ' ', text)
        text = re.sub("\*|\°|\_|\+|\&|\/", ' ',text) 
        # Normalize space
        text = re.sub("\s+", ' ', text)
        # Remove hyphen
        text = re.sub("-",' ', text)
        # Remove accents
        if not self.keep_accent:
            text = remove_accents(text)
        # Remove special character
        if not self.keep_accent:
            text = remove_accents(text)
        # Eliminate stopwords
        text = [
            word for word in text.split(' ')
            if word not in self.stop_words and len(word) > 1
        ]
        return text
    
    def lemmatized(self, word):
        """Get lemmas of words."""
        if word in self.morphalou._lemmas_dictionary.keys():
            word = self.morphalou._lemmas_dictionary[word]
        return(word)
    
def remove_accents(text):
    """Replace accentuated characters with unaccentuated ones."""
    charmap = [
            ('àâ', 'a'), ('éèêë', 'e'), ('ùûü', 'u'),
            ('îï', 'i'), ('ôö', 'o'),
        ]
    for character in charmap:
        text = re.sub('[%s]' % character[0], character[1], text)
    return text

def remove_special_char(text):
    """Replace special characters with corresponding not special ones."""
    charmap = [
            ('ç', 'c'), ('œ', 'oe'), ('æ','ae'), ('@','a')
        ]
    for character in charmap:
        text = re.sub('[%s]' % character[0], character[1], text)
    return text