# -*- coding: utf-8 -*-
"""
Created on Mon Dec 17 15:55:40 2018

@author: claire.dumesnildemar
"""

#from spellchecker import SpellChecker
from bisect import bisect_left
import re
from sympound import sympound
import platform

class FuzzyMatcher():
    
    """Class to fuzzy match strings"""
    
    def __init__(
            self, lexicon_file = None, lexicon = None, synonyms_dict = None, 
            acronyms_dict = None
            ):
        # Initialize lexicon
        if lexicon_file is not None:
            self.lexicon_file = lexicon_file
            self._load_autocomplete()
        else:
            self.lexicon = lexicon
        self.lexicon = sorted(self.lexicon)
        # Initialize Levenshtein Spell check
        self._init_levenshtein()
        if synonyms_dict is not None:
            self.synonyms = synonyms_dict
        if acronyms_dict is not None:
            self.acronyms = acronyms_dict

        #self.lexicon_file = lexicon_file
        # Initialize spell checking
#        self.lexicon_file = lexicon_file
#        self.spell = SpellChecker(language=None)
#        self.spell.word_frequency.load_text_file(self.lexicon_file)


    ###############################################################
    #                  Handle autocompletion
    ###############################################################
    def _load_autocomplete(self):
        self.lexicon = []
        with open(self.lexicon_file) as lexicon:
            for line in lexicon.readlines():
                self.lexicon += [re.sub('[\n]', '',str(line))]
        lexicon.close()
        
        
    def _autocomplete(self, token, len_autocomplete = 3):
        """Return possible autocompletion for a word"""
        if len(token) > len_autocomplete-1:
            try:
                return(self.lexicon[bisect_left(self.lexicon, token):
                     bisect_left(self.lexicon, token[:-1] + chr(ord(token[-1])+1))])
            except IndexError:
                return([token])
        else:
            return([token])

    ###############################################################
    #                Levenshtein spell check
    ###############################################################
    def _init_levenshtein(self, dict_path = None):
        distancefun = None
        if platform.system() != "Windows":
            from pyxdameraulevenshtein import damerau_levenshtein_distance
            distancefun = damerau_levenshtein_distance
        else:
            from jellyfish import levenshtein_distance
            distancefun = levenshtein_distance
        ssc = sympound(
                distancefun=distancefun, 
                maxDictionaryEditDistance=3
                )
        for word in self.lexicon:
            ssc.words[word] = 1
        self.ssc = ssc
#        self.ssc.load_dictionary(r'C:/Users/claire.dumesnildemar/Documents/PCS_Labels_manager/data/pcs_freq.txt')
        
    def _levenshtein(self, token, dist = 1):
        """Return all words from the lexicon at a distance of dist of token."""
        return({
                sug.term 
                for sug in self.ssc.lookup(
                        input_string=token, 
                        edit_distance_max=dist, 
                        verbosity=2
                        )
                })
        
#    def _get_single_candidates_levenshtein(self, token, distance = 1):
#        # Must at least return token
#        self.spell.distance = distance
#        return(self.spell.candidates(token))
#        
#    def get_candidates_levenshtein(self, tokens, distance = 1):
#        """ Function to return all possible combinaison of tokens with 
#        acceptable spelling.
#        Example:    
#            [vedeur, habillement]
#        return 
#            [vendeur, habillement]
#            [videur, habillement]
#        """
#        self.spell.distance = distance
#        results = []
#        for token in tokens:
#            results.append (
#                    list(self._get_single_candidates_levenshtein(token))
#                    )
#        return(
#                [
#                    list(tokens) 
#                    for tokens in list(itertools.product(*results))
#                ]
#            )
        # Must eliminate the original combination
    


#    def get_candidates_autocomplete(self, tokens, len_autocomplete = 3):
#        """
#        Return possible list of tokens when using autocompletion
#        It only performs autocompletion for the last word and if its length is 
#        superior to len_autocomplete
#        len_autocomplete : min length to perform autocompletion (int default 3)
#        """
#        results = [
#                tokens[:-1] + [autocomplete]
#                for autocomplete in self._autocomplete(tokens[-1])
#                ]
#        return(results)

    def _get_single_candidate_synonyms(self, token):
        """Return possible synonyms for a word."""
        # Must at least return token
        return(token)
    
    def get_candidates_synonyms(self, tokens):
        """Return possible list of tokens when using synonyms."""
        results = []
        return(results)
    
    def _get_single_candidate_acronyms(self, token):
        """Return possible acronyms for a word."""
        # Be careful : tokens must be separated
        # Must at least return token
        return(token)
    
    def get_candidates_acronyms(self, tokens):
        """Return possible list of tokens when using acronyms."""
        results = []
        return(results)