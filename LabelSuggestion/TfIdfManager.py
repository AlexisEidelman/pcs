# -*- coding: utf-8 -*-
"""
Created on Wed Dec 19 14:40:18 2018

@author: claire.dumesnildemar
"""

from gensim import corpora, models
from gensim.matutils import cossim

from StringCleaner import StringCleaner

class TfIdfManager():
    """ Manage Tf-idf for documents. """
    
    def __init__(self, documents, cleaner = None):
        if cleaner is None:
            self.cleaner = StringCleaner()
        self._get_corpus(documents)
        self._tfidf()
        
    def _get_corpus(self, documents):
        """ Get a Gensim compatible corpus."""
        # Get nice array of array of clean documents
        titles = [
                [word for word in self.cleaner.clean_string(doc)] 
                for doc in documents
                ]
        # Transform it into a dictionnary
        dictionary = corpora.Dictionary(titles)
        self.dictionary = dictionary
        self.num_features = len(dictionary.dfs.keys())
        # Finally get a corpus
        self.corpus = [dictionary.doc2bow(title) for title in titles]
        
    def _tfidf(self):
        """Get tfidf for corpus of documents."""
        self.tfidf = models.TfidfModel(self.corpus)
        
    def _vec_transform(self,tokens):
        """
        Transform a string to match in a vector to be compatible with corpus.
        """
        vec = self.dictionary.doc2bow(tokens)
        return vec
    
    def similarity_cosine(self, tokens_a, tokens_b):
        """Calculate cosine similarity between two sets of tokens."""
        vec_a = self.tfidf[self._vec_transform(tokens_a)]
        vec_b = self.tfidf[self._vec_transform(tokens_b)]
        similarity  = cossim(vec_a,vec_b)
        return similarity
    
    def similarity_cosine_unique(self, tokens_a, tokens_b):
        """Calculate cosine similarity between two sets of tokens."""
        vec_a = self.tfidf[self._vec_transform(list(set(tokens_a)))]
        vec_b = self.tfidf[self._vec_transform(list(set(tokens_b)))]
        similarity  = cossim(vec_a,vec_b)
        return similarity