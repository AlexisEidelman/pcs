# -*- coding: utf-8 -*-
"""
Created on Mon Dec 17 10:50:37 2018

@author: claire.dumesnildemar
"""

from StringCleaner import StringCleaner
from FuzzyMatcher import FuzzyMatcher
from TfIdfManager import TfIdfManager
import pickle
import os
import itertools
from more_itertools import unique_everseen
from jellyfish import levenshtein_distance, jaro_winkler

"""
Nielsen's time scale explanation for UX:

Instant results:

0.1 second is the response time limit if you want users to feel like their 
actions are directly causing something to happen on the screen . For example, 
if you click on an expandable menu and see the expanded version in less than 
0.1 seconds, then it feels as if you made the menu open up.

To create the illusion of direct manipulation , a user interface must be faster
 than 0.1 second.

1 sec results:

When the computer takes more than 0.1 second but less than 1 second to respond 
to your input, it feels like the computer is causing the result to appear. 
Although users notice the short delay, they stay focused on their current train
 of thought during the one-second interval.

This means that during 1-second response times, users retain the feeling of 
being in control of the interaction even though they notice that it's a 2-way 
interaction (between them and the computer). By contrast, with 0.1 second 
response times, users simply feel like they're doing something themselves.

"""

class SearchEngine():
    
    def __init__(self, cleaner = None, data_series = None, indexes_path = None,
                 export_index = None, export = False, export_path = None):
        # Initialize cleaner
        if cleaner is None:
            cleaner = StringCleaner()
        self.cleaner = cleaner
        # Load index and data
        if indexes_path is not None:
            self.load_indexes(indexes_path)
            print("Index proccessed.")
        elif data_series is not None:
            self.create_indexes(data_series)
            if export:
                self.export_indexes(export_path)
            print("Data series proccessed.")
        else:
            print("No reference data provided, could not initialize search engine.")
            pass
        # Init Tf Idf
        # A refaire si on exporte les index et que data_series = None
        self.tfidfmodel = TfIdfManager(data_series.unique())
        # Load lexicon and init fuzzy matcher
        self.lexicon = [self.cleaner.clean_string(x)[0] for x in self.index.keys()]
        self.fuzzy_matcher = FuzzyMatcher(lexicon = self.lexicon)
    
    ###############################################################
    #                       Manage indexes
    ###############################################################
    
    def create_indexes(self, data_series):
        """
        Create an index and store it in a file.
        data_series : data series of strings (pandas.Series)
        export_index  :
        Return a dict with the following format:
            dict = { 
                    "vendeur" : [[1,[3]],[2,[4,5]], ...]
                    ...
            }
        """
        # Create a dict mapping words to labels id
        index = {}
        for label_id, label in enumerate(data_series.unique()):
            label_tokens = self.cleaner.clean_string(label)
            for token_id, token in enumerate(label_tokens):
                # Word is already in dict
                if token in index:
                    # Multiple times the same word in document
                    if label_id in index[token]:
                        index[token][label_id].append(token_id)
                    else :
                        index[token][label_id] = [token_id]
                # Word not in dict
                else:
                    index[token] = {label_id:[token_id]} 
        self.index = index
        # Create a dict mapping labels id to labels
        revert_index = {}
        for label_id, label in enumerate(data_series.unique()):
            revert_index[label_id] = label
        self.revert_index = revert_index
    
    def export_indexes(self, export_path):
        # Export index
        index_file = os.path.join(export_path,'index.txt')
        revert_index_file =  os.path.join(export_path,'revert_index.txt')
        with open(index_file, 'wb') as index:
            pickle.dump(self.index,index)
        with open(revert_index_file, 'wb') as revert_index:
            pickle.dump(self.revert_index,revert_index)
 
    def load_indexes(self, import_path):   
        index_file = os.path.join(import_path,'index.txt')
        revert_index_file =  os.path.join(import_path,'revert_index.txt')
        with open(index_file, 'rb') as index:
            self.index = pickle.load(index)
        with open(revert_index_file, 'rb') as revert_index:
            self.revert_index = pickle.load(revert_index)
            
    def _revert_id(self, label_id):
        """Get label out of a label id."""
        label = self.revert_index[label_id]
        return(label)
    
    ###############################################################
    #                       Manage queries
    ###############################################################    
    
    def handleUserQuery(self, query, max_output = 10, ranking = None):
        """Take string provided by user and return list of best match for 
        labels from index.
        query       : (str)
        max_results : max number of matching results to be returned (int 
                      default 10)
        ranking     : how to rank results (function default None)
        """
        # Clean user input
        query_tokens = self.cleaner.clean_string(query)
        # Get other possible tokens arr (token based method)
        possible_tokens = self._get_possible_tokens(query_tokens)
        # Get all labels with matching words
        labels_match = []
        for tokens_arr in possible_tokens:
            labels_match += self._get_match(tokens_arr, query_tokens)
        rank_labels = [x[1] for x in sorted(labels_match, reverse=True)]
        unique_rank_labels = list(unique_everseen(rank_labels))
        return(unique_rank_labels[:max_output])
#        return(sorted(labels_match, reverse=True))
    
    def _get_possible_tokens(self,tokens):
        """Get possible tokens corresponding to query tokens.
        
        >>> se._get_possible_tokens(['oficier','polic'])
        >>>[['officier', 'police'], ['officier', 'policier']]
        
        For now:
            * spell check each words with levenshtein distance 2
            * autocomplete last word
        (beside cleaning)
        """
        results = []
        for token in tokens[:-1]:
            set_tokens = self.fuzzy_matcher._levenshtein(token)
            results.append(list(set_tokens))
        # Autocomplete for last word    
        set_tokens = set(self.fuzzy_matcher._autocomplete(tokens[-1]))
        set_tokens |= self.fuzzy_matcher._levenshtein(tokens[-1])
        results.append(list(set_tokens))
        # Get all possible tokens
        possible_tokens = [
                    list(tokens) 
                    for tokens in list(itertools.product(*results))
                ]
        return(possible_tokens)
    
    def _get_match(self, tokens, query_tokens):
        """Return matching labels for tokens and corresponding score"""
        results = set()
        # Get all labels with matching words
        for token in tokens:
            try:
                results |= set(self.index[token].keys()) 
            except:
                pass
        # Get matching labels
        matches = []
        for label_id in results:
            label = self._revert_id(label_id)
            label_tokens = self.cleaner.clean_string(label)
            score = self.get_score(label_tokens, tokens, query_tokens)
            matches += [(score,label)]
        return(matches)

    ###############################################################
    #                       Rank matches
    # Work in progress ...
    ###############################################################
    def get_score(self,label_tokens, tokens, query_tokens):
        # Cost from query_tokens to tokens
        score_jw = jaro_winkler(''.join(query_tokens),''.join(tokens))
        # cost from tokens to label_tokens
        score_tfidf = self.tfidfmodel.similarity_cosine_unique(
                label_tokens, 
                tokens
                )
#        if len(tokens) > 1:
#            score_query = self.tfidfmodel.similarity_cosine_unique(
#                    label_tokens, 
#                    query_tokens[:-1] + [tokens[-1]]
#                    )
#        else:
#            score_query = self.tfidfmodel.similarity_cosine_unique(
#                    label_tokens, 
#                    query_tokens
#                    )
#        # Importance of common words in labels an tokens
#        score_words = len(set(label_tokens).intersection(tokens)) / (len(tokens))
        # Weigth of first words
#        score_first = 0#0.3*(label_tokens[0]==tokens[0])
        # Number of exact words as input
        score_exact = len(set(label_tokens).intersection(query_tokens)) / (len(query_tokens))
        return(score_tfidf + score_jw + score_exact)# + score_words + score_first + score_query)# + score_exact)
#        return(score_tfidf + score_levenshtein)